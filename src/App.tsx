import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import ErrantryTool from './components/ErrantryTool';
import useGoogleAnalytics from './analytics-hook';

const Routes = () => {
  useGoogleAnalytics();

  return (
    <Switch>
      <Route path ="/" exact component={ErrantryTool}>
        <ErrantryTool />
      </Route>
    </Switch>
  )
}

const App = () => {  
  return (
    <Router>
      <Routes />
    </Router>
  );
}

export default App;
