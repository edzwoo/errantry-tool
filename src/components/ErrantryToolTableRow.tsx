import React from 'react'

const ErrantryToolTableRow = ( { loy, lif, pow, int, ski, spd, def, breed, location, disabledAbilities, abilityData, addAbilities, removeAbilities }:any ) => {
  var autoLearnChance = (abilityData.autoLearnChance) ? abilityData.autoLearnChance : "";
  var lifS = "";
  var powS = "";
  var intS = "";
  var skiS = "";
  var spdS = "";
  var defS = "";
  var minThreshold = "";
  var maxThreshold = "";
  var badNature = "";
  var goodNature = "";
  var subReq: string[] = [];
  var subLock: string[] = [];
  var techName = "";
  var techCount = "";
  if (abilityData.statParameters) {
    lifS = (abilityData.statParameters.lif) ? "X" : "";
    powS = (abilityData.statParameters.pow) ? "X" : "";
    intS = (abilityData.statParameters.int) ? "X" : "";
    skiS = (abilityData.statParameters.ski) ? "X" : "";
    spdS = (abilityData.statParameters.spd) ? "X" : "";
    defS = (abilityData.statParameters.def) ? "X" : "";
    minThreshold = abilityData.statParameters.minThreshold;
    maxThreshold = abilityData.statParameters.maxThreshold;
  }
  if (abilityData.lockoutParameters) {
    badNature = (abilityData.lockoutParameters.badNature) ? abilityData.lockoutParameters.badNature : "";
    goodNature = (abilityData.lockoutParameters.goodNature) ? abilityData.lockoutParameters.goodNature : "";
    techName = (abilityData.lockoutParameters.techUsage) ? abilityData.lockoutParameters.techUsage.techApiName : "";
    techCount = (abilityData.lockoutParameters.techUsage) ? abilityData.lockoutParameters.techUsage.count : "";
    subReq = (abilityData.lockoutParameters.subRequired) ? abilityData.lockoutParameters.subRequired : [];
    subLock = (abilityData.lockoutParameters.subLockout) ? abilityData.lockoutParameters.subLockout : [];
  }
  
  var odds = 100;
  var statTotal = parseInt(loy);
  if (abilityData.statParameters) {
    odds = 0;
    var difference = parseInt(maxThreshold) - parseInt(minThreshold);
    if (abilityData.statParameters.lif) {
      statTotal += parseInt(lif)
    }
    if (abilityData.statParameters.pow) {
      statTotal += parseInt(pow)
    }
    if (abilityData.statParameters.int) {
      statTotal += parseInt(int)
    }
    if (abilityData.statParameters.ski) {
      statTotal += parseInt(ski)
    }
    if (abilityData.statParameters.spd) {
      statTotal += parseInt(spd)
    }
    if (abilityData.statParameters.def) {
      statTotal += parseInt(def)
    }
    if (statTotal >= parseInt(minThreshold)) {
      odds = Math.floor(10000 * ((difference - (parseInt(maxThreshold) - statTotal)) / (difference * 1.0))) / 100.0
      if (odds > 100) {
        odds = 100;
      }
    }
  }

  const subReqCheck = () => {
    if (subReq.length !== 0) {
      return ( <span>✔</span> )
    }
  };

  const subLockCheck = () => {
    if (subLock.length !== 0) {
      return ( <span>✔</span> )
    }
  };

  const subReqList = subReq.map( breed => {
    return (
      <div>
        {breed}
      </div>
    )
  });

  const subLockList = subLock.map( breed => {
    return (
      <div>
        {breed}
      </div>
    )
  });


  const buttonCheck = () => {
    var list = disabledAbilities as string[];
    if (!list.includes(abilityData.abilityApiName)) {
      return (
        <button onClick={() => addAbilities(abilityData.abilityApiName)} className="w-16 bg-gray-500 text-xs text-black px-1 border-black border">
          Enabled
        </button>
      )
    } else {
      return (
        <button onClick={() => removeAbilities(abilityData.abilityApiName)} className="w-16 bg-gray-200 text-xs text-black px-1 border-black border">
          Disabled
        </button>
      )
    }
  };


  return (
    <tr>
      <td className="whitespace-no-wrap">
        <div className="text-sm text-gray-900">{abilityData.abilityApiName}</div>
      </td>
      <td className="whitespace-no-wrap">
        <div className="text-sm text-center text-gray-900">{breed}</div>
      </td>
      <td className="whitespace-no-wrap">
        <div className="text-sm text-center text-gray-900">{location}</div>
      </td>
      <td className="whitespace-no-wrap">
        <div className="text-sm text-center text-gray-900">{autoLearnChance ? autoLearnChance+"%" : ""}</div>
      </td>
      <td className={ lifS ? "w-6 border whitespace-no-wrap bg-yellow-400" : "w-6 border whitespace-no-wrap"}>
      </td>
      <td className={ powS ? "w-6 border whitespace-no-wrap bg-red-600" : "w-6 border whitespace-no-wrap"}>
      </td>
      <td className={ intS ? "w-6 border whitespace-no-wrap bg-green-400" : "w-6 border whitespace-no-wrap"}>
      </td>
      <td className={ skiS ? "w-6 border whitespace-no-wrap bg-pink-500" : "w-6 border whitespace-no-wrap"}>
      </td>
      <td className={ spdS ? "w-6 border whitespace-no-wrap bg-blue-200" : "w-6 border whitespace-no-wrap"}>
      </td>
      <td className={ defS ? "w-6 border whitespace-no-wrap bg-indigo-700" : "w-6 border whitespace-no-wrap"}>
      </td>
      <td className="whitespace-no-wrap">
        <div className="text-sm text-center text-gray-900">{minThreshold}</div>
      </td>
      <td className="whitespace-no-wrap">
        <div className="text-sm text-center text-gray-900">{maxThreshold}</div>
      </td>
      <td className="whitespace-no-wrap">
        <div className="text-sm text-center font-bold text-gray-900">{statTotal}</div>
      </td>
      <td className="whitespace-no-wrap">
        <div className="text-sm text-right text-gray-900 pr-4">{ odds+"%"}</div>
      </td>
      <td className="whitespace-no-wrap">
        <div className="text-sm text-right text-gray-900 mr-3">{ badNature || goodNature }</div>
      </td>
      <td className="whitespace-no-wrap">
        <div className="text-sm text-right text-blue-500 pr-1">{ techCount }</div>
      </td>
      <td className="whitespace-no-wrap">
        <div className="text-sm text-left text-gray-900">{ techName }</div>
      </td>
      <td className="whitespace-no-wrap">
        <div className="text-sm text-center tooltip">
          { subReqCheck() }<span className='tooltip-text bg-gray-200 p-6 -mt-6 -ml-6 rounded'>{ subReqList }</span>
        </div>
      </td>
      <td className="whitespace-no-wrap">
        <div className="text-sm text-center tooltip">
          { subLockCheck() }<span className='tooltip-text bg-gray-200 p-6 -mt-6 -ml-6 rounded'>{ subLockList }</span>
        </div>
      </td>
      <td className="whitespace-no-wrap justify-items-center">
        <div className="text-center">
          { buttonCheck() }
        </div>
      </td>
    </tr>
  )
}

export default ErrantryToolTableRow;