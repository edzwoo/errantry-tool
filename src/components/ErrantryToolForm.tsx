import React from 'react';

const ErrantryToolForm = ( { setLoy, setLif, setPow, setInt, setSki, setSpd, setDef }:any ) => {
  return (
    <div>
      <div className="flex justify-center text-2xl font-bold text-center text-gray-900">
        Monster Stats
      </div>
      <div>
        Loy
        <input onChange={e => setLoy(e.target.value)} className="w-20 text-gray-700 py-1 px-2 border-b border-blue-500 leading-tight focus:outline-none" type="number" min="0" max="100"/>
        Lif
        <input onChange={e => setLif(e.target.value)} className="w-20 text-gray-700 py-1 px-2 border-b border-blue-500 leading-tight focus:outline-none" type="number" min="1" max="999"/>
        Pow
        <input onChange={e => setPow(e.target.value)} className="w-20 text-gray-700 py-1 px-2 border-b border-blue-500 leading-tight focus:outline-none" type="number" min="1" max="999"/>
        Int
        <input onChange={e => setInt(e.target.value)} className="w-20 text-gray-700 py-1 px-2 border-b border-blue-500 leading-tight focus:outline-none" type="number" min="1" max="999"/>
        Ski
        <input onChange={e => setSki(e.target.value)} className="w-20 text-gray-700 py-1 px-2 border-b border-blue-500 leading-tight focus:outline-none" type="number" min="1" max="999"/>
        Spd
        <input onChange={e => setSpd(e.target.value)} className="w-20 text-gray-700 py-1 px-2 border-b border-blue-500 leading-tight focus:outline-none" type="number" min="1" max="999"/>
        Def
        <input onChange={e => setDef(e.target.value)} className="w-20 text-gray-700 py-1 px-2 border-b border-blue-500 leading-tight focus:outline-none" type="number" min="1" max="999"/>
	    </div>
    </div>
    )
}

export default ErrantryToolForm;