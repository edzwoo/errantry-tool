import React from 'react'

const BreedCard = () => {
  return (
    <div className="mb-4 px-2">
      <img className="object-contain h-96 w-full" src="https://img.atwikiimg.com/www35.atwiki.jp/mf2_matome/attach/71/511/durahan.jpg" />
      <div className="text-center text-gray-700 text-xl mb-2">Durahan</div>
    </div>
  )
}

export default BreedCard;

/*

*/