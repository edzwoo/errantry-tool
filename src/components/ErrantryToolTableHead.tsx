import React from 'react'

const ErrantryToolTableHead = () => {
  return (
    <thead>
      <tr>
        <th className="bg-gray-50 text-left text-xxs font-medium text-gray-500 leading-relaxed uppercase">
          Ability
        </th>
        <th className="bg-gray-50 text-center text-xxs font-medium text-gray-500 leading-relaxed uppercase">
          Breed
        </th>
        <th className="bg-gray-50 text-center text-xxs font-medium text-gray-500 leading-relaxed uppercase">
          Location
        </th>
        <th className="bg-gray-50 text-center text-xxs font-medium text-gray-500 leading-relaxed uppercase">
          AutoLearn
        </th>
        <th className="bg-gray-50 text-center text-xxs font-medium text-gray-500 leading-relaxed uppercase">
          Lif
        </th>
        <th className="bg-gray-50 text-center text-xxs font-medium text-gray-500 leading-relaxed uppercase">
          Pow
        </th>
        <th className="bg-gray-50 text-center text-xxs font-medium text-gray-500 leading-relaxed uppercase">
          Int
        </th>
        <th className="bg-gray-50 text-center text-xxs font-medium text-gray-500 leading-relaxed uppercase">
          Ski
        </th>
        <th className="bg-gray-50 text-center text-xxs font-medium text-gray-500 leading-relaxed uppercase">
          Spd
        </th>
        <th className="bg-gray-50 text-center text-xxs font-medium text-gray-500 leading-relaxed uppercase">
          Def
        </th>
        <th className="bg-gray-50 text-center text-xxs font-medium text-gray-500 leading-relaxed uppercase">
          0%+ 
        </th>
        <th className="bg-gray-50 text-center text-xxs font-medium text-gray-500 leading-relaxed uppercase">
          100%
        </th>
        <th className="bg-gray-50 text-center text-xxs font-medium text-gray-500 leading-relaxed uppercase">
          Input
        </th>
        <th className="bg-gray-50 text-center text-xxs font-medium text-gray-500 leading-relaxed uppercase">
          Chance
        </th>
        <th className="bg-gray-50 text-center text-xxs font-medium text-gray-500 leading-relaxed uppercase">
          Nature
        </th>
        <th className="bg-gray-50 text-center text-xxs font-medium text-gray-500 leading-relaxed uppercase">
          #
        </th>
        <th className="bg-gray-50 text-left text-xxs font-medium text-gray-500 leading-relaxed uppercase">
          Tech Req Name
        </th>
        <th className="bg-gray-50 text-center text-xxs font-medium text-gray-500 leading-relaxed uppercase">
          Sub Req
        </th>
        <th className="bg-gray-50 text-center text-xxs font-medium text-gray-500 leading-relaxed uppercase">
          Sub Lock
        </th>
        <th className="bg-gray-50 text-center text-xxs font-medium text-gray-500 leading-relaxed uppercase">
          Learnable
        </th>
      </tr>
    </thead>
  )
}

export default ErrantryToolTableHead;