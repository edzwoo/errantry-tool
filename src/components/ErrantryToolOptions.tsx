import React from 'react';

const ErrantryToolOptions = ( { breeds, locationSelect, breedSelect, removeAllAbilities }:any ) => {
  var locations:string[] = ["Torble","Papas","Mandy","Parepare","Kawrea"]

  const breedList = breeds.map( (breed:string) => {
    return (
      <button className="hover:bg-black text-black-dark font-semibold hover:text-white px-2 mb-2 border border-black hover:border-transparent rounded-full mr-2" onClick={() => breedSelect(breed)}>
        {breed}
      </button>
    )
  });
  
  const errantryList = locations.map( (location) => {
    return (
      <button className="hover:bg-black text-black-dark font-semibold hover:text-white px-2 mb-2 border border-black hover:border-transparent rounded-full mr-2" onClick={() => locationSelect(location)}>
        {location}
      </button>
    )
  });

  return (
    <div>
      <div className="flex justify-center text-2xl font-bold text-center text-gray-900 py-4">
        Options
      </div>
      <div>
        <span className="text-lg font-bold text-center text-gray-900 mr-5">Breed:</span>
        <button className="hover:bg-black text-black-dark font-semibold hover:text-white px-2 border border-black hover:border-transparent rounded-full mr-2" onClick={() => breedSelect('')}>
          All
        </button>
        { breedList }
	    </div>
      <div>
        <span className="text-lg font-bold text-center text-gray-900 mr-5">Errantry:</span>
        <button className="hover:bg-black text-black-dark font-semibold hover:text-white px-2 border border-black hover:border-transparent rounded-full mr-2" onClick={() => locationSelect('')}>
          All
        </button>
        { errantryList }
      </div>
      <div>
        <span className="text-lg font-bold text-center text-gray-900 mr-5">Abilities:</span>
        <button onClick={() => removeAllAbilities()} className="hover:bg-black text-black-dark font-semibold hover:text-white px-2 mb-2 border border-black hover:border-transparent rounded-full mr-2">
          Enable All
        </button>
      </div>
    </div>
    )
}

export default ErrantryToolOptions;