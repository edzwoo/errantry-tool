import React from 'react'

const ErrantryToolExactOdds = ( { loy, lif, pow, int, ski, spd, def, errantryTable }:any ) => {
  var abilityOdds = [];
  var abilityNames = [];
  var nameOdds: {name: string, odds: number}[] = [];
  var currentOddsModifier = 1;
  for (var i = 0; i < errantryTable.length; i++) {
    abilityNames.push(errantryTable[i].abilityApiName);
    var odds = 1;
    var statTotal = parseInt(loy);
    if (errantryTable[i].statParameters) {
      odds = 0;
      var minThreshold = parseInt(errantryTable[i].statParameters.minThreshold);
      var maxThreshold = parseInt(errantryTable[i].statParameters.maxThreshold);
      var difference = maxThreshold - minThreshold;
      if (errantryTable[i].statParameters.lif) {
        statTotal += parseInt(lif)
      }
      if (errantryTable[i].statParameters.pow) {
        statTotal += parseInt(pow)
      }
      if (errantryTable[i].statParameters.int) {
        statTotal += parseInt(int)
      }
      if (errantryTable[i].statParameters.ski) {
        statTotal += parseInt(ski)
      }
      if (errantryTable[i].statParameters.spd) {
        statTotal += parseInt(spd)
      }
      if (errantryTable[i].statParameters.def) {
        statTotal += parseInt(def)
      }
      if (statTotal >= minThreshold) {
        odds = (difference - (maxThreshold - statTotal)) / (difference * 1.0)
        if (odds > 1) {
          odds = 1;
        }
      }
    }

    var dodgeLearn = 1 - odds;
    var dodgeAndHitAutoLearn = 0;
    if (errantryTable[i].autoLearnChance) {
      dodgeAndHitAutoLearn = dodgeLearn * (parseInt(errantryTable[i].autoLearnChance) / 100.0)
    }
    var autoLearnAdjustOdds = odds + dodgeAndHitAutoLearn;
    var currentOdds = autoLearnAdjustOdds * currentOddsModifier
    abilityOdds.push(Math.floor(currentOdds * 10000) / 100.0)
    nameOdds.push({name: (errantryTable[i].abilityApiName), odds: (Math.floor(currentOdds * 10000) / 100.0)})
    currentOddsModifier = currentOddsModifier - currentOdds;
  };

  const abilityOddsList = nameOdds.map( ({name, odds}) => {
    return (
      <tr>
        <td className="whitespace-no-wrap">
          <div className="text-sm text-left text-gray-900">{name}</div>
        </td>
        <td className="whitespace-no-wrap">
          <div className="text-sm text-right text-gray-900">{odds+"%"}</div>
        </td>
      </tr>
    );
  });

  return (
    <div>
      <div className="flex justify-center text-2xl font-bold text-center text-gray-900">
        Exact Odds
      </div>
      <div className="flex justify-center py-2">
        <table className="table-fixed divide-y divide-gray-200">
          <thead>
            <tr>
              <th className="bg-gray-50 text-center text-xxs font-medium text-gray-500 leading-relaxed uppercase">
                Ability
              </th>
              <th className="bg-gray-50 text-center text-xxs font-medium text-gray-500 leading-relaxed uppercase">
                Odds
              </th>
            </tr>
          </thead>
          <tbody className="bg-white">
            { abilityOddsList }
          </tbody>
        </table>
      </div>
    </div>
  )
}

export default ErrantryToolExactOdds;