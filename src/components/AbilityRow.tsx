import React from 'react'

const AbilityRow = () => {
  return (
    <tr>
      <td className="px-6 py-4 whitespace-no-wrap">
        <div className="flex items-center">
          <div className="flex-shrink-0 h-12 w-18">
            <img className="h-12 w-18" src="https://img.atwikiimg.com/www35.atwiki.jp/mf2_matome/attach/86/1233/%E3%83%91%E3%83%B3%E3%83%81.jpg" alt=""></img>
          </div>
        </div>
      </td>
      <td className="px-6 py-4 whitespace-no-wrap">
        <div className="text-sm leading-5 font-medium text-gray-900">Punch</div>
      </td>
      <td className="px-6 py-4 whitespace-no-wrap">
        <div className="text-sm leading-5 text-gray-900">1</div>
      </td>
      <td className="px-6 py-4 whitespace-no-wrap">
        <div className="text-sm leading-5 text-gray-900">Basic</div>
      </td>
      <td className="px-6 py-4 whitespace-no-wrap">
        <div className="text-sm leading-5 text-gray-900">Pow</div>
      </td>
      <td className="px-6 py-4 whitespace-no-wrap">
        <div className="text-sm leading-5 text-gray-900">Neutral</div>
      </td>
      <td className="px-6 py-4 whitespace-no-wrap">
        <div className="text-sm leading-5 text-gray-900">10</div>
      </td>
      <td className="px-6 py-4 whitespace-no-wrap">
        <div className="text-sm leading-5 text-gray-900">14</div>
      </td>
      <td className="px-6 py-4 whitespace-no-wrap">
        <div className="text-sm leading-5 text-gray-900">9</div>
      </td>
      <td className="px-6 py-4 whitespace-no-wrap">
        <div className="text-sm leading-5 text-gray-900">-</div>
      </td>
      <td className="px-6 py-4 whitespace-no-wrap">
        <div className="text-sm leading-5 text-gray-900">-</div>
      </td>
      <td className="px-6 py-4 whitespace-no-wrap">
        <div className="text-sm leading-5 text-gray-900">Punch: 50 times</div>
        <div className="text-sm leading-5 text-gray-900">Sub Required: Durahan</div>
        <div className="text-sm leading-5 text-gray-900">Sub Excluded: Henger</div>
      </td>
    </tr>
  )
}

export default AbilityRow;

/*

      <div className="flex flex-col">
        <div className="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
          <div className="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
            <div className="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">
              <table className="min-w-full divide-y divide-gray-200">
                <thead>
                  <tr>
                    <th className="px-6 py-3 bg-gray-50 text-left text-xxs leading-4 font-medium text-gray-500 uppercase tracking-wider">
                      Icon
                    </th>
                    <th className="px-6 py-3 bg-gray-50 text-left text-xxs leading-4 font-medium text-gray-500 uppercase tracking-wider">
                      Name
                    </th>
                    <th className="px-6 py-3 bg-gray-50 text-left text-xxs leading-4 font-medium text-gray-500 uppercase tracking-wider">
                      Priority
                    </th>
                    <th className="px-6 py-3 bg-gray-50 text-left text-xxs leading-4 font-medium text-gray-500 uppercase tracking-wider">
                      Lif
                    </th>
                    <th className="px-6 py-3 bg-gray-50 text-left text-xxs leading-4 font-medium text-gray-500 uppercase tracking-wider">
                      Pow
                    </th>
                    <th className="px-6 py-3 bg-gray-50 text-left text-xxs leading-4 font-medium text-gray-500 uppercase tracking-wider">
                      Int
                    </th>
                    <th className="px-6 py-3 bg-gray-50 text-left text-xxs leading-4 font-medium text-gray-500 uppercase tracking-wider">
                      Ski
                    </th>
                    <th className="px-6 py-3 bg-gray-50 text-left text-xxs leading-4 font-medium text-gray-500 uppercase tracking-wider">
                      Spd
                    </th>
                    <th className="px-6 py-3 bg-gray-50 text-left text-xxs leading-4 font-medium text-gray-500 uppercase tracking-wider">
                      Def
                    </th>
                    <th className="px-6 py-3 bg-gray-50 text-left text-xxs leading-4 font-medium text-gray-500 uppercase tracking-wider">
                      Total
                    </th>
                    <th className="px-6 py-3 bg-gray-50 text-left text-xxs leading-4 font-medium text-gray-500 uppercase tracking-wider">
                      Difference
                    </th>
                    <th className="px-6 py-3 bg-gray-50 text-left text-xxs leading-4 font-medium text-gray-500 uppercase tracking-wider">
                      Ignore %
                    </th>
                    <th className="px-6 py-3 bg-gray-50 text-left text-xxs leading-4 font-medium text-gray-500 uppercase tracking-wider">
                      Nature
                    </th>
                    <th className="px-6 py-3 bg-gray-50 text-left text-xxs leading-4 font-medium text-gray-500 uppercase tracking-wider">
                      Tech Req
                    </th>
                    <th className="px-6 py-3 bg-gray-50 text-left text-xxs leading-4 font-medium text-gray-500 uppercase tracking-wider">
                      Tech Usage
                    </th>
                    <th className="px-6 py-3 bg-gray-50 text-left text-xxs leading-4 font-medium text-gray-500 uppercase tracking-wider">
                      Sub Requirement
                    </th>
                    <th className="px-6 py-3 bg-gray-50 text-left text-xxs leading-4 font-medium text-gray-500 uppercase tracking-wider">
                      Sub Lockout
                    </th>
                  </tr>
                </thead>
                <tbody className="bg-white divide-y divide-gray-200">
                  <LearningRow />
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>


*/