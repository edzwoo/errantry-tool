import React from 'react'

const ErrantryToolDisabledAbilities = ( { abilities, removeAllAbilities, removeAbilities }:any ) => {
  const abilityList = abilities.map( (ability: string) => {
    return (       
      <button onClick={() => removeAbilities(ability)} className="hover:bg-black text-black-dark font-semibold hover:text-white px-2 mb-2 border border-black hover:border-transparent rounded-full mr-2">
        {ability}
      </button>
    )
  })

  return (
    <div>
      <div className="flex justify-center text-2xl font-bold text-center text-gray-900">
        Disabled Abilities
      </div>
      <div>
        <button onClick={() => removeAllAbilities()} className="hover:bg-black text-black-dark font-semibold hover:text-white px-2 mb-2 border border-black hover:border-transparent rounded-full mr-2">
          Clear All
        </button>
        { abilityList }
      </div>
    </div>
  )
}

export default ErrantryToolDisabledAbilities;