import React from 'react'
import BreedCard from './BreedCard';

function BreedList() {
  return (
    <div className="mt-6 px-6">
      <div className="flex flex-wrap flex-col">
        <div className="flex flex-wrap">
          <BreedCard />
          <BreedCard />
          <BreedCard />
          <BreedCard />
          <BreedCard />
          <BreedCard />
          <BreedCard />
          <BreedCard />
          <BreedCard />
          <BreedCard />
          <BreedCard />
          <BreedCard />
          <BreedCard />
          <BreedCard />
          <BreedCard />
          <BreedCard />
          <BreedCard />
          <BreedCard />
          <BreedCard />
          <BreedCard />
        </div>
      </div>
    </div>
  )
}

export default BreedList;
