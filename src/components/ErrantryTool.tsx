import React, { useState, /*useEffect*/ } from 'react'
import {Helmet} from 'react-helmet';
import ErrantryToolForm from './ErrantryToolForm';
//import ErrantryToolDisabledAbilities from './ErrantryToolDisabledAbilities';
import ErrantryToolExactOdds from './ErrantryToolExactOdds';
import ErrantryToolOptions from './ErrantryToolOptions';
import ErrantryToolTableRow from './ErrantryToolTableRow';
import ErrantryToolTableHead from './ErrantryToolTableHead';
import abilityJSON from '../assets/ErrantryData.json'

const ErrantryTool = () => {
  const [breed, setBreed] = useState('');
  const [errantry, setErrantry] = useState('');
  const [disabledAbilities, setDisabledAbilties] = useState<string[]>([]);
  const [loy, setLoy] = useState(0);
  const [lif, setLif] = useState(0);
  const [pow, setPow] = useState(0);
  const [int, setInt] = useState(0);
  const [ski, setSki] = useState(0);
  const [spd, setSpd] = useState(0);
  const [def, setDef] = useState(0);

  const table = Object.entries(abilityJSON)
  .filter( ([breedJSON, locationSets]) => 
    breed === '' || breed === breedJSON
  )
  .map( ([breed, locationSets]) => {
    return (
      Object.entries(locationSets)
      .filter( ([location, abilities]) => 
        errantry === '' || errantry === location
      )
      .map( ([location, abilities]) => {
        return (
          Object.values(abilities).map( abilityData => {
            return (
              <ErrantryToolTableRow loy={loy} lif={lif} pow={pow} int={int} ski={ski} spd={spd} def={def}
                                    breed={breed} location={location} abilityData={abilityData} disabledAbilities={disabledAbilities}
                                    addAbilities={ (ability: string) => setDisabledAbilties(disabledAbilities => [...disabledAbilities, ability])}
                                    removeAbilities={ (ability: string) => setDisabledAbilties(disabledAbilities.filter((e) => (e !== ability)))} />
            )
          })
        )
      })
    )
  })
  
  const oddsTable = () => {
    if (breed && errantry) {
      var errantryTable = abilityJSON as any;
      errantryTable = errantryTable[breed][errantry].filter( (abilityData: { abilityApiName: string; }) => 
        !disabledAbilities.includes(abilityData.abilityApiName)
      )
      return (
        <ErrantryToolExactOdds loy={loy} lif={lif} pow={pow} int={int} ski={ski} spd={spd} def={def} errantryTable={errantryTable} />  
      )
    }
  }

  /*
  useEffect(() => {
    Object.values(abilityJSON).forEach(errantries => {
      Object.values(errantries).forEach( abilities => {
        Object.values(abilities).forEach( (abilityData: any) => {
          if (abilityData.lockoutParameters !== undefined) {
            setDisabledAbilties(disabledAbilities => [...disabledAbilities, abilityData.abilityApiName as string])
          }
        })
      })
    })
  }, []);
  */

  return (
    <div className="container mx-auto py-8">
      <Helmet>
        <title>Monster Rancher 2 Errantry Tool</title>
        <meta name="description" content="Monster Rancher 2 Errantry Tool gives you the exact odds to learn the abilties you need." />
      </Helmet>
      <div className="flex justify-center">
        <div className="flex justify-center text-2xl font-bold text-center text-gray-900">
          How to Use
        </div>
      </div>
      <div className="flex justify-center py-4 border-b-2">
        <ol className="list-disc">
          <li>The 0%+ column means when you initially meet the learning requirements. 100% is when you guarantee learning the ability.</li>
          <li>The INPUT is determined by the sum of the highlighted stat columns as well as LOYALTY.</li>
          <li>CHANCE is your odds of learning based of your Stat Parameters and nothing else.</li>
          <li>Many techniques have an AUTOLEARN percent, which means it will ignore the stat parameters. <b>This does NOT ignore the other checks.</b></li>
          <li><b>To get the exact odds table to show, select both Breed and Errantry.</b></li>
          <li>You will have to manually Disable techs that are unlearnable due to Nature, Tech Req Usage, Sub Reqs and Locks.</li>
          <li>Highlight the ✔ to see which SubBreeds are required or locked out.</li>
          <br></br>
          <p><i>All data is credited to SmilingFaces96</i></p>
        </ol>
      </div>
      <div className="flex justify-center py-4 border-b-2">
        <ErrantryToolOptions breeds={Object.keys(abilityJSON)} 
                             locationSelect={(btn:string) => setErrantry(btn)} breedSelect={(btn:string) => setBreed(btn)}
                             removeAllAbilities={() => setDisabledAbilties([])} />
      </div>
      <div className="flex justify-center py-4">
        <ErrantryToolForm setLoy={(value:number) => setLoy(value)} setLif={(value:number) => setLif(value)} setPow={(value:number) => setPow(value)} setInt={(value:number) => setInt(value)} setSki={(value:number) => setSki(value)} setSpd={(value:number) => setSpd(value)} setDef={(value:number) => setDef(value)} />
      </div>
      {/*
      <div className="flex justify-center py-4 border-b-2">
        <ErrantryToolDisabledAbilities abilities={disabledAbilities}
                                       removeAllAbilities={() => setDisabledAbilties([])}
                                       removeAbilities={ (ability: string) => setDisabledAbilties(disabledAbilities.filter((e) => (e !== ability)))} />
      </div>
      */}
      <div className="flex justify-center py-4">
        <table className="table-fixed divide-y divide-gray-200">
          <ErrantryToolTableHead />
          <tbody className="bg-white">
            { table }
          </tbody>
        </table>
      </div>
      <div className="flex justify-center">
        { oddsTable() }
      </div>
    </div>
  )
}

export default ErrantryTool;